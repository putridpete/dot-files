# Dot Files

Just a compilation of my configs.

## Screenshots

![alt text](https://peterdominguez.art/wp-content/uploads/2025/01/1.png "Display 1")
![alt text](https://peterdominguez.art/wp-content/uploads/2025/01/2.png "Display 2")

## Color Scheme

I use the [Dracula theme](https://draculatheme.com/), following their [color palette](https://github.com/dracula/dracula-theme) as best as I can. 

## Software

* OS: [Arch Linux](https://archlinux.org/)
* Boot loader: [Systemd-boot](https://www.freedesktop.org/wiki/Software/systemd/systemd-boot/)
* Display Manager: [None](https://wiki.hyprland.org/Getting-Started/Master-Tutorial/#launching-hyprland)
* Networking: [NetworkManager](https://networkmanager.dev/)
* Wayland Compositor: [Hyprland](https://github.com/hyprwm/Hyprland)
* Bar: [Waybar](https://github.com/Alexays/Waybar)
* Notifications: [SwayNotificationCenter](https://github.com/ErikReider/SwayNotificationCenter)
* Launcher: [Rofi](https://github.com/DaveDavenport/rofi)
* Menu Editor: [alacarte](https://gitlab.gnome.org/GNOME/alacarte)
* File Manager: [nnn](https://github.com/jarun/nnn)
* Text Editor: [Vim](https://github.com/vim/vim)
* PDF Reader: [Zathura](https://pwmt.org/projects/zathura)
* Screenshot Tool: [grim](https://git.sr.ht/~emersion/grim)+[slurp](https://github.com/emersion/slurp)
* Screen Recording: [OBS Studio](https://obsproject.com)
* Screen Lock: [Hyprlock](https://github.com/hyprwm/hyprlock)
* Idle Manager: [Hypridle](https://github.com/hyprwm/hypridle)
* Fonts: [ttf-droid](https://www.droidfonts.com), [awesome-terminal-fonts](https://github.com/gabrielelana/awesome-terminal-fonts), [noto-fonts](https://fonts.google.com/noto), [gnu-free-fonts](https://www.gnu.org/software/freefont)
* Terminal: [Kitty](https://github.com/kovidgoyal/kitty)
* Web browser: [Firefox](https://www.mozilla.org/firefox)
* Music player: [Feishin](https://github.com/jeffvli/feishin)
* Video player: [mpv](https://github.com/mpv-player/mpv)
* System information tool: [HyFetch](https://github.com/hykilpikonna/hyfetch)
* System monitor: [Btop](https://github.com/aristocratos/btop)
* Shell: [zsh](https://www.zsh.org/)
* Prompt: [starship](https://github.com/starship/starship)
* Fetch Tool: [fastfetch](https://github.com/fastfetch-cli/fastfetch)

## Browser theme

* [Dracula Darker Theme](https://addons.mozilla.org/en-US/firefox/addon/dracula-darker-theme): Firefox browser theme with dracula colors.

## Browser addons

* [ff2mpv](https://addons.mozilla.org/en-US/firefox/addon/ff2mpv): A Firefox/Chrome add-on for playing URLs in mpv.
* [uBlock Origin](https://addons.mozilla.org/addon/ublock-origin): An efficient blocker for Chromium and Firefox. Fast and lean.
* [KeePassXC-Browser](https://addons.mozilla.org/en-US/firefox/addon/keepassxc-browser): Official browser plugin for the KeePassXC password manager.
* [Vimium](https://addons.mozilla.org/en-GB/firefox/addon/vimium-ff): The hacker's browser.
* [LibRedirect](https://addons.mozilla.org/firefox/addon/libredirect): A web extension that redirects several websites to alternative privacy friendly frontends.
[floccus](https://addons.mozilla.org/en-US/firefox/addon/floccus): Sync bookmarks privately across browsers and devices.
* [FF-Container-HomePages](https://github.com/MoAllabbad/FF-Container-HomePages): Firefox browser extension that lets you specify a default page for each container.

## zsh plugins

* [zsh-syntax-highlighting](https://github.com/zsh-users/zsh-syntax-highlighting): Fish shell like syntax highlighting for zsh.
* [zsh-autosuggestions](https://github.com/zsh-users/zsh-autosuggestions): Fish-like autosuggestions for zsh.
* [zsh-history-substring-search](https://github.com/zsh-users/zsh-history-substring-search): zsh port of Fish history search with arrow keys.

## Vim plugins

* [vim-airline](https://github.com/vim-airline/vim-airline): Status line, written in Vimscript.
* [vim-airline-themes](https://github.com/vim-airline/vim-airline-themes): Themes for vim-airline.
* [vim-supertab](https://github.com/ervandew/supertab): Use the tab key to do all insert completion.
* [vim-lastplace](https://github.com/farmergreg/vim-lastplace): Intelligently reopen files at your last edit position.

## Other Misc. Tools

* [hyprpaper](https://github.com/hyprwm/hyprpaper): A blazing fast wayland wallpaper utility with IPC controls.
* [fzf](https://github.com/junegunn/fzf): Command-line fuzzy finder.
* [newsboat](https://github.com/newsboat/newsboat): RSS/Atom feed reader for the text console.
* [yt-dlp](https://github.com/yt-dlp/yt-dlp): Download videos from youtube or other video sites.
* [mpv-mpris](https://github.com/hoyon/mpv-mpris): Plugin for mpv which allows control of it using standard media keys.
* [yt-dlp-drop-in](https://aur.archlinux.org/packages/yt-dlp-drop-in): Emulates youtube-dl executables with yt-dlp.
* [gsimplecal](https://dmedvinsky.github.io/gsimplecal/): A lightweight calendar application written in C++ using GTK.
* [qt5ct](https://sourceforge.net/projects/qt5ct/): Qt5 configuration utility.
* [kvantum](https://github.com/tsujan/Kvantum): SVG-based theme engine for Qt5/6.
* [polkit-kde-agent](https://github.com/KDE/polkit-kde-agent-1): Daemon providing a polkit authentication UI.
* [xdg-desktop-portal-gtk](https://github.com/flatpak/xdg-desktop-portal-gtk): A backend implementation for xdg-desktop-portal using GTK.
* [trash-cli](https://github.com/andreafrancia/trash-cli): Command line trashcan (recycle bin) interface.
* [arkenfox-user.js](https://github.com/arkenfox/user.js): Firefox user.js template for configuration and hardening.
* [ttyper](https://github.com/max-niederman/ttyper): Terminal-based typing test.
* [glow](https://github.com/charmbracelet/glow): Command-line markdown renderer. 
* [udiskie](https://pypi.python.org/pypi/udiskie): Removable disk automounter using udisks.
* [blueman](https://github.com/blueman-project/blueman): GTK+ Bluetooth Manager.

## Credits

A lot of the code was modified or borrowed from others that I will attempt to list here: 

* dpgraham4401's excelent [dot files](https://github.com/dpgraham4401/.dotfiles).
* theCode-Breaker's [dot files](https://github.com/theCode-Breaker/riverwm/tree/main/waybar/river) as well.
* Surendrajat's [waybar weather module](https://gist.github.com/Surendrajat/ff3876fd2166dd86fb71180f4e9342d7).
* lobianco's [tv_rename.sh script](https://gist.github.com/lobianco/8599868).
* MrRoy's swaync [style.css](https://gist.github.com/MrRoy/3a4a0b1462921e78bf667e1b36697268) and his [config.json](https://gist.github.com/MrRoy/40f103bc34f3a58699e218c3d06d1a43).
*  Chick2D's [neofetch-themes](https://github.com/Chick2D/neofetch-themes) repository.
*  MrVivekRajan's [themes for Hyprlock](https://github.com/MrVivekRajan/Hyprlock-Styles), which I used as base for mine.

If I'm missing anybody else, just open up a MR.
