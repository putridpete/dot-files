packadd! dracula
syntax enable
colorscheme dracula
set number noswapfile hlsearch ignorecase incsearch cursorline
inoremap kj <ESC>
let mapleader = "'"
let g:airline_theme='selenized'
let g:airline_powerline_fonts='1'
let g:airline#extensions#tabline#enabled='1'
autocmd BufNewFile,BufRead ~/.config/waybar/config,*.json set ft=javascript
hi Normal guibg=NONE ctermbg=NONE
highlight CursorLine ctermbg=black
