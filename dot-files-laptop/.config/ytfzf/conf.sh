#!/bin/bash
YTFZF_ENABLE_FZF_DEFAULT_OPTS=1
FZF_DEFAULT_OPTS="--color=border:magenta,info:green,header:green,info:yellow,hl:blue,label:magenta"
show_thumbnails=1
load_thumbnail_viewer kitty
thumbnail_video_info_text () {
	printf "\n${c_cyan}%s" "$title"
        printf "\n "
	printf "\n${c_blue}Channel ${c_green}%s" "$channel"
	printf "\n${c_blue}Duration ${c_yellow}%s" "$duration"
	printf "\n${c_blue}Views ${c_magenta}%s" "$views"
	printf "\n${c_blue}Date ${c_cyan}%s" "$date"
        printf "\n "
}
