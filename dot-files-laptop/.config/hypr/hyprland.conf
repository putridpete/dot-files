# ┌───────────────────────────────────────────────────┐
# │ ██████╗ ███████╗████████╗███████╗██████╗ ███████╗ │
# │ ██╔══██╗██╔════╝╚══██╔══╝██╔════╝██╔══██╗██╔════╝ │
# │ ██████╔╝█████╗     ██║   █████╗  ██████╔╝███████╗ │
# │ ██╔═══╝ ██╔══╝     ██║   ██╔══╝  ██╔══██╗╚════██║ │
# │ ██║     ███████╗   ██║   ███████╗██║  ██║███████║ │
# │ ╚═╝     ╚══════╝   ╚═╝   ╚══════╝╚═╝  ╚═╝╚══════╝ │
# ├───────────────────────────────────────────────────┤
# │  depressing and totally messy hyprland config or  │
# ├───────────────────────────────────────────────────┤
# │          How windows are the only well            │
# │           managed aspect of my life.              │
# │---------------------------------------------------│
# │     https://gitlab.com/putridpete/dot-files       │
# └───────────────────────────────────────────────────┘
#
#

# Programs 

$terminal = kitty
$menu = rofi -show drun

# Some default env vars

env = XCURSOR_SIZE,24
env = QT_QPA_PLATFORMTHEME,qt5ct # change to qt6ct if you have that

# Outputs

monitor=eDP-1,1920x1080@60,0x0,1

# trigger when the switch is toggled

bindl=,switch:Lid Switch,exec,swaylock -f

# Screen brightness

bind = , XF86MonBrightnessUp, exec, brightnessctl s +10%
bind = , XF86MonBrightnessDown, exec, brightnessctl s 10%-

# Input devices 

input {
    kb_layout = us(altgr-intl)
    kb_variant = ,nodeadkeys
    kb_options = caps:escape

    follow_mouse = 1

    sensitivity = 0 # -1.0 - 1.0, 0 means no modification.
}

device {
     name = tpps/2-ibm-trackpoint
     sensitivity = 0.7
}

device {
     name = synaptics-tm3276-022
     sensitivity = 0.3
}

# Executables

exec-once = dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP
exec-once = udiskie -t &
exec-once = /usr/lib/polkit-kde-authentication-agent-1 
exec-once = sleep 1; blueman-applet
exec-once = waybar
exec-once = swaync
exec-once = hyprpaper
exec-once = hypridle
exec-once = hyprctl setcursor Dracula-cursors 24
exec-once = [workspace 1 silent] firefox
exec-once = libreoffice --writer --norestore
exec-once = gimp
exec-once = [workspace 2 silent] kitty btop
exec-once = [workspace 2 silent] sleep 1; kitty 
exec-once = [workspace 2 silent] sleep 1;kitty --session ~/.config/kitty/session --listen-on=unix:@mykitty
exec-once = [workspace special:password silent] keepassxc 
exec-once = [workspace special:scratchpad silent] signal-desktop --ozone-platform-hint=auto --enable-features=UseOzonePlatform,WaylandWindowDecorations 
exec-once = nextcloud
exec-once = killall nextcloud
exec-once = [workspace special:music silent] sleep 1; feishin --enable-features=UseOzonePlatform --ozone-platform=wayland
exec-once = gammastep -m wayland -O 6500K -g 1.1.0
exec-once = [workspace special:email silent] sleep 1; tutanota-desktop --ozone-platform-hint=auto
exec-once = discord --enable-features=UseOzonePlatform --ozone-platform=wayland --start-minimized
exec-once = jellyfin-mpv-shim

# Variables

general {
    gaps_in = 5
    gaps_out = 10
    border_size = 2
    layout = master
    allow_tearing = false
}

decoration {

    rounding = 8

   blur {
        enabled = false
        size = 3 
        passes = 1
        new_optimizations = true
        noise = 0.06
        special = true
        contrast = 0.8 
        popups = true
    }

}

# Animations

animations {
    enabled = yes

    bezier = myBezier, 0, 0.55, 0.45, 1

    animation = windows, 1, 4, myBezier
    animation = windowsIn, 1, 7, default, popin 80%
    animation = windowsOut, 1, 7, default, popin 80%
    animation = border, 1, 20, default
    animation = borderangle, 1, 20, default
    animation = fade, 1, 7, default
    animation = workspaces, 1, 6, default, slide
}

# Layout

master {
    new_status = slave 
}

# Cursor 

cursor {
    hide_on_key_press = true
}

# Miscellaneous

misc {
    force_default_wallpaper = 0 # Set to 0 to disable the anime mascot wallpapers
    vfr = true
}

# Window rules

windowrulev2 = workspace 3 silent,class:libreoffice-writer
windowrulev2 = workspace 4 silent,class:gimp
windowrulev2 = workspace special:email silent,class:tutanota-desktop
windowrulev2 = float,class:nmnet
windowrulev2 = size 860 540,class:nmnet
windowrulev2 = float,class:com.nextcloud.desktopclient.nextcloud
windowrulev2 = move 1450 38,class:com.nextcloud.desktopclient.nextcloud
windowrulev2 = float,class:openrgb
windowrulev2 = size 1600 900,class:openrgb
windowrulev2 = center,class:openrgb
windowrulev2 = float,class:org.pulseaudio.pavucontrol
windowrulev2 = move 1310 40,class:org.pulseaudio.pavucontrol
windowrulev2 = float,title:foot
windowrulev2 = float,title:Save Files*
windowrulev2 = size 1000 600,title:Save Files*
windowrulev2 = float,title:Open Files
windowrulev2 = size 1000 600,title:Open Files
windowrulev2 = float,title:Signal
windowrulev2 = center,title:Signal
windowrulev2 = size 1280 720,title:Signal
windowrulev2 = float,class:feishin
windowrulev2 = center,class:feishin
windowrulev2 = float,class:org.keepassxc.KeePassXC
windowrulev2 = size 800 600,class:org.keepassxc.KeePassXC
windowrulev2 = center,class:org.keepassxc.KeePassXC
windowrulev2 = float,class:mpv
windowrulev2 = monitor DP-1,class:mpv
windowrulev2 = size 600 400,class:mpv
windowrulev2 = move 1310 670,class:mpv
windowrulev2 = center,class:discord
windowrulev2 = float,class:discord
windowrulev2 = size 1600 900, class:discord
windowrulev2 = float,class:org.kde.polkit-kde-authentication-agent-1
windowrulev2 = float,class:alacarte
windowrulev2 = float,class:nwg-look
windowrulev2 = float,class:zenity
windowrulev2 = float,class:galculator
windowrulev2 = move 1580 730,class:galculator
windowrulev2 = float,class:qsynth
windowrulev2 = float,class:qt5ct
windowrulev2 = float,class:kvantummanager
windowrulev2 = float,class:nm-connection-editor
windowrulev2 = float,class:udiskie
windowrulev2 = float,class:gsimplecal
windowrulev2 = move 800 43,class:gsimplecal
windowrulev2 = float,class:imv
windowrulev2 = center,class:imv
windowrulev2 = float,class:org.pwmt.zathura
windowrulev2 = center,class:org.pwmt.zathura
windowrulev2 = idleinhibit fullscreen, fullscreen:1
windowrulev2 = float,class:tutanota-desktop 
windowrulev2 = center,class:tutanota-desktop
windowrulev2 = size 1705 909,class:tutanota-desktop
windowrulev2 = float,class:xdg-desktop-portal-gtk
windowrulev2 = float,class:blueman-manager
windowrulev2 = center,class:blueman-manager
windowrulev2 = move 1310 38,class:blueman-manager
windowrulev2 = size 600 900,class:blueman-manager

# Keywords

$mainMod = SUPER

# Bindings 

bind = $mainMod, Return, exec, $terminal
bind = $mainMod, Q, killactive, 
bind = $mainMod, E, exit, 
bind = $mainMod, V, togglefloating, 
bind = $mainMod, P, exec, $menu
bind = $mainMod, F, fullscreen
bind = $mainMod, C, exec, galculator
bind = $mainMod, n, exec, exec swaync-client -t -sw
bind = $mainMod CTRL, S, exec, [workspace special:scratchpad silent] signal-desktop --ozone-platform-hint=auto --enable-features=UseOzonePlatform,WaylandWindowDecorations 
bind = $mainMod CTRL, P, exec, [workspace special:password silent] keepassxc
bind = $mainMod CTRL, M, exec, [workspace special:music silent] sleep 1; feishin --enable-features=UseOzonePlatform --ozone-platform=wayland

# Resize windows main keybind
bind=ALT,R,submap,resize

# Start a submap resize
submap=resize

# Set repeatable binds for resizing the active window
binde=,right,resizeactive,10 0
binde=,left,resizeactive,-10 0
binde=,up,resizeactive,0 -10
binde=,down,resizeactive,0 10
binde=,l,resizeactive,10 0
binde=,h,resizeactive,-10 0
binde=,k,resizeactive,0 -10
binde=,j,resizeactive,0 10

# Use reset to go back to the global submap
bind=,escape,submap,reset 

# Reset the submap
submap=reset

# Switch between floating and tiled windows

bind = ALT, TAB, cyclenext, floating
bind = ALT, SPACE, focuscurrentorlast

# Media keys

bind = ,XF86AudioPlay,exec,playerctl play-pause
bind = ,XF86AudioPause,exec,playerctl pause
bind = ,XF86AudioStop,exec,playerctl stop
bind = ,XF86AudioNext,exec,playerctl next
bind = ,XF86AudioPrev,exec,playerctl previous
bind = ,XF86AudioRaiseVolume,exec,wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%+
bind = ,XF86AudioLowerVolume,exec,wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%-
bind = ,XF86AudioMute,exec,pactl set-sink-mute @DEFAULT_SINK@ toggle

# Move focus with Mod + arrow keys

bind = $mainMod, left, movefocus, l
bind = $mainMod, right, movefocus, r
bind = $mainMod, up, movefocus, u
bind = $mainMod, down, movefocus, d

# Move windows with Mod + arrow keys

bind = $mainMod CTRL, left, movewindow, l
bind = $mainMod CTRL, right, movewindow, r
bind = $mainMod CTRL, up, movewindow, u
bind = $mainMod CTRL, down, movewindow, d

# VI keybindings like God intended

bind = $mainMod, h, movefocus, l
bind = $mainMod, l, movefocus, r
bind = $mainMod, k, movefocus, u
bind = $mainMod, j, movefocus, d

# Move windows with Mod + vi keys

bind = $mainMod CTRL, h, movewindow, l
bind = $mainMod CTRL, l, movewindow, r
bind = $mainMod CTRL, k, movewindow, u
bind = $mainMod CTRL, j, movewindow, d

# Switch workspaces with Mod + [0-8]

bind = $mainMod, 1, workspace, 1
bind = $mainMod, 2, workspace, 2
bind = $mainMod, 3, workspace, 3
bind = $mainMod, 4, workspace, 4
bind = $mainMod, 5, workspace, 5
bind = $mainMod, 6, workspace, 6
bind = $mainMod, 7, workspace, 7
bind = $mainMod, 8, workspace, 8

# Move active window to a workspace with Mod + CTRL + [0-8]

bind = $mainMod CTRL, 1, movetoworkspace, 1
bind = $mainMod CTRL, 2, movetoworkspace, 2
bind = $mainMod CTRL, 3, movetoworkspace, 3
bind = $mainMod CTRL, 4, movetoworkspace, 4
bind = $mainMod CTRL, 5, movetoworkspace, 5
bind = $mainMod CTRL, 6, movetoworkspace, 6
bind = $mainMod CTRL, 7, movetoworkspace, 7
bind = $mainMod CTRL, 8, movetoworkspace, 8

# Workspace monitor assignment

workspace = 1, monitor:eDP-1, default:true
workspace = 2, monitor:eDP-1
workspace = 3, monitor:eDP-1
workspace = 4, monitor:eDP-1
workspace = 5, monitor:eDP-1
workspace = 6, monitor:eDP-1
workspace = 7, monitor:eDP-1, default:true
workspace = 8, monitor:eDP-1

# Special workspaces

bind = $mainMod, U, togglespecialworkspace, scratchpad
bind = $mainMod SHIFT, S, movetoworkspace, special:scratchpad

bind = $mainMod, M, togglespecialworkspace, music 
bind = $mainMod, O, togglespecialworkspace, password 
bind = $mainMod, I, togglespecialworkspace, email

# Scroll through existing workspaces with Mod + scroll

bind = $mainMod, mouse_down, workspace, e+1
bind = $mainMod, mouse_up, workspace, e-1

# Scroll through existing workspaces with Mod + vi keys 

bind = $mainMod ALT, l, workspace, e+1
bind = $mainMod ALT, h, workspace, e-1

# Scroll through existing workspaces with Mod + arrow keys 

bind = $mainMod ALT, right, workspace, e+1
bind = $mainMod ALT, left, workspace, e-1

# Move/resize windows with Mod + LMB/RMB and dragging

bindm = $mainMod, mouse:272, movewindow
bindm = $mainMod, mouse:273, resizewindow

# Screenshot 

bind = ,Print,exec,grim -g "$(slurp)" - | swappy -f -

# Dracula theme

general {
    col.active_border = rgb(6272A4) rgb(bd93f9) 90deg
    col.inactive_border = rgba(44475aaa)
    col.nogroup_border = rgba(282a36dd)
    col.nogroup_border_active = rgb(bd93f9) rgb(44475a) 90deg
    no_border_on_floating = false
    border_size = 2

}
decoration:shadow {
    color = rgba(1E202966)
    enabled = true
    range = 60
    offset = 1 2
    render_power = 3
    scale = 0.97
}

group {
    groupbar {
        col.active = rgb(bd93f9) rgb(44475a) 90deg
        col.inactive = rgba(282a36dd)
    }
}
windowrulev2 = bordercolor rgb(ff5555),xwayland:1 # check if window is xwayland
