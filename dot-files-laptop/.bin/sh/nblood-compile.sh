#!/bin/sh

# Switch to git directory and clone source
{
cd /home/peter/Downloads/git;
git clone https://github.com/nukeykt/NBlood.git;
}

# Build binary

{
cd NBlood;
make blood;
}

# Install executable @ ~/.bin

{
cd /home/peter/.bin/NBlood;
mv /home/peter/.bin/NBlood/nblood /home/peter/.bin/NBlood/old;
mv /home/peter/.bin/NBlood/nblood.pk3 /home/peter/.bin/NBlood/old;
cd /home/peter/Downloads/git/NBlood;
cp /home/peter/Downloads/git/NBlood/nblood /home/peter/.bin/NBlood;
cp /home/peter/Downloads/git/NBlood/nblood.pk3 /home/peter/.bin/NBlood;
chmod 755 /home/peter/.bin/NBlood/nblood;
}

# Cleanup

{
rm -rf /home/peter/Downloads/git/NBlood;
}
