#!/bin/sh
auracle search --quiet --searchby=name $1 | fzf --color="border:magenta,info:green,header:green,info:yellow,hl:blue,label:magenta" --multi --preview "auracle info {1}" --preview-window=up | xargs -ro auracle clone -C ~/Downloads/AUR
