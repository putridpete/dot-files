#!/bin/sh
pacman -Qq | fzf --color="border:magenta,info:green,header:green,info:yellow,hl:blue,label:magenta" --multi --preview "pacman -Qi {1}" --preview-window=up | xargs -ro sudo pacman -R
