#!/bin/sh

# Switch to git directory and clone source
{
cd /home/peter/Downloads/git;
git clone https://voidpoint.io/terminx/eduke32.git;
}

# Build binary

{
cd eduke32;
make voidsw;
}

# Install executable @ ~/.bin

{
cd /home/peter/.bin/voidsw;
mv /home/peter/.bin/voidsw/voidsw /home/peter/.bin/voidsw/old;
cd /home/peter/Downloads/git/eduke32;
cp /home/peter/Downloads/git/eduke32/voidsw /home/peter/.bin/voidsw;
chmod 755 /home/peter/.bin/voidsw/voidsw;
}

# Cleanup

{
rm -rf /home/peter/Downloads/git/eduke32;
}
