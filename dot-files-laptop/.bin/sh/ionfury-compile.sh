#!/bin/sh

# Switch to git directory and clone source
{
cd /home/peter/Downloads/git;
git clone https://voidpoint.io/terminx/eduke32.git;
}

# Build binary

{
cd eduke32;
make;
}

# Install executable @ ~/.bin

{
cd /home/peter/.bin/eduke32;
mv /home/peter/.bin/eduke32/eduke32 /home/peter/.bin/eduke32/old;
mv /home/peter/.bin/eduke32/mapster32 /home/peter/.bin/eduke32/old;
cd /home/peter/Downloads/git/eduke32;
cp /home/peter/Downloads/git/eduke32/eduke32 /home/peter/.bin/eduke32;
cp /home/peter/Downloads/git/eduke32/mapster32 /home/peter/.bin/eduke32;
chmod 755 /home/peter/.bin/eduke32/eduke32;
}

# Cleanup

{
rm -rf /home/peter/Downloads/git/eduke32;
}
