#!/bin/sh

# Prompt Reboot

{
read -p "Shutdown now? [y/n]:" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    command shutdown -h now
fi
}
