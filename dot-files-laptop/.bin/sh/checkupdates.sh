#!/bin/sh 

# Check for system updates

{
echo -e "\e[94mChecking Official packages updates...
\e[39m-------------------------------------
"
checkupdates
echo -e "\e[91m
Checking AUR packages updates...
\e[39m--------------------------------
"
auracle outdated
}

# Perform system check

{
read -p "Check for errors and failed services? [y/n]:" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    systemctl --failed && journalctl -p 3 -b
fi 
}

# Perform update

{
read -p "Perform system update? [y/n]:" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    ~/.bin/sh/update.sh
fi
}
