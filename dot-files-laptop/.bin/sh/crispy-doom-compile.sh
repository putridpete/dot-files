#!/bin/sh

# Switch to git directory and clone source
{
cd /home/peter/Downloads/git;
git clone https://github.com/fabiangreffrath/crispy-doom.git;
}

# Build binary

{
cd crispy-doom;
autoreconf -fiv;
./configure;
make;
}

# Install executable @ ~/.bin

{
cd /home/peter/.bin/crispy-doom;
mv /home/peter/.bin/crispy-doom/crispy-doom /home/peter/.bin/crispy-doom/old;
mv /home/peter/.bin/crispy-doom/crispy-doom-setup /home/peter/.bin/crispy-doom/old;
cd /home/peter/Downloads/git/crispy-doom;
cp /home/peter/Downloads/git/crispy-doom/src/crispy-doom /home/peter/.bin/crispy-doom;
cp /home/peter/Downloads/git/crispy-doom/src/crispy-doom-setup /home/peter/.bin/crispy-doom;
chmod 755 /home/peter/.bin/crispy-doom/crispy-doom;
}

# Cleanup

{
rm -rf /home/peter/Downloads/git/crispy-doom;
}
