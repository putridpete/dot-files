#!/bin/sh
pacman -Slq | fzf --color="border:magenta,info:green,header:green,info:yellow,hl:blue,label:magenta" --multi --preview "pacman -Si {1}" --preview-window=up | xargs -ro sudo pacman -S
