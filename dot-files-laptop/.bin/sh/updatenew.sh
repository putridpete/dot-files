#!/bin/sh

# Check for updates

{
read -p "Check for updates? [y/n]:" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    ~/.bin/sh/checkupdates.sh
fi 
}

# Perform system check

{
read -p "Check for errors and failed services? [y/n]:" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    systemctl --failed && journalctl -p 3 -b
fi 
}

# Perform update

{
read -p "Perform system update? [y/n]:" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    sudo pacman -Syu;
fi
}

# Check pacdiff

{
read -p "Execute pacdiff? [y/n]:" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    sudo pacdiff
fi
}

# Prompt reboot

~/.bin/sh/reboot.sh
