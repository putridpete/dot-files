#!/bin/sh

# Build package and prompt for cleaning

{
makepkg -si;
read -p "Clean leftover files? [y/n]:" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    git clean -dxfi
fi
}
