# ┌───────────────────────────────────────────────────┐
# │ ██████╗ ███████╗████████╗███████╗██████╗ ███████╗ │
# │ ██╔══██╗██╔════╝╚══██╔══╝██╔════╝██╔══██╗██╔════╝ │
# │ ██████╔╝█████╗     ██║   █████╗  ██████╔╝███████╗ │
# │ ██╔═══╝ ██╔══╝     ██║   ██╔══╝  ██╔══██╗╚════██║ │
# │ ██║     ███████╗   ██║   ███████╗██║  ██║███████║ │
# │ ╚═╝     ╚══════╝   ╚═╝   ╚══════╝╚═╝  ╚═╝╚══════╝ │
# ├───────────────────────────────────────────────────┤
# │ no good, total trash and possibly broken ~/.zshrc │
# ├───────────────────────────────────────────────────┤
# │   Anime traps and dank memes, one command away!   │
# │---------------------------------------------------│
# │     https://gitlab.com/putridpete/dot-files       │
# └───────────────────────────────────────────────────┘

# 1. Autoload

autoload -Uz compinit history-substring-search-up history-substring-search-down bracketed-paste-url-magic && compinit
zstyle ':completion:*' menu select # Auto completion
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'
zle -N bracketed-paste bracketed-paste-url-magic # autoquote urls

# 2. Auto correction

setopt CORRECT

# 3. History search

zle -N history-substring-search-up
zle -N history-substring-search-down

# 4. History settings

export HISTFILE="$HOME/.zsh_history"
export HISTSIZE=10000
export SAVEHIST=10000
setopt EXTENDED_HISTORY
setopt INC_APPEND_HISTORY

HISTORY_SUBSTRING_SEARCH_HIGHLIGHT_FOUND=true

# 5. Git repository

alias config='/usr/bin/git --git-dir=$HOME/dot-files/.git/ --work-tree=$HOME'

# 6. Starship

eval "$(starship init zsh)"

# 7. nnn variables

BLK="04" CHR="04" DIR="04" EXE="00" REG="00" HARDLINK="00" SYMLINK="06" MISSING="00" ORPHAN="01" FIFO="0F" SOCK="0F" OTHER="02"
export NNN_FCOLORS="$BLK$CHR$DIR$EXE$REG$HARDLINK$SYMLINK$MISSING$ORPHAN$FIFO$SOCK$OTHER" NNN_COLORS="2345"
export NNN_TRASH=1
export NNN_ARCHIVE="\\.(7z|a|ace|alz|arc|arj|bz|bz2|cab|cpio|deb|gz|jar|lha|lz|lzh|lzma|lzo|rar|rpm|rz|t7z|tar|tbz|tbz2|tgz|tlz|txz|tZ|tzo|war|xpi|xz|Z|zip)$"
export NNN_PLUG='p:preview-tui;c:x2sel'
export SPLIT='h'
export NNN_FIFO='/tmp/nnn.fifo'
export NNN_BMS='d:~/Downloads/;u:~/;n:~/Nextcloud;w:~/Nextcloud/Notes;c:~/.config;r:/run/media;a:~/Downloads/AUR;!:~/.bin/sh;t:~/.local/share/Trash/files;y:/media/NFS'
export VISUAL='vim'

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# 8. Aliases
#    8.1 zsh aliases

alias ls='nnn -deUxH'
alias aurdir='cd ~/Downloads/AUR'
alias gitdir='cd ~/Downloads/git'
alias notesdir='cd ~/Nextcloud/Notes'
alias mv='mv -i'
alias cp='cp -i'
alias rm='rm -i'
alias ..='cd ..'
alias ...='cd ../..'
alias visudo='sudo EDITOR=vim visudo'
export EDITOR=vim

#    8.2 Sway aliases

alias lock='~/.bin/sh/swaylock.sh'
alias logoff='swaymsg exit'
alias gettree='swaymsg -pt get_tree'
alias getoutputs='hyprctl monitors all'

#    8.3 System commands aliases

alias amdman='echo "manual" | sudo tee /sys/class/drm/card0/device/power_dpm_force_performance_level'
alias amdhigh='echo "high" | sudo tee /sys/class/drm/card0/device/power_dpm_force_performance_level'
alias gpumode='cat /sys/class/drm/card0/device/pp_power_profile_mode'
alias systemcheck='systemctl --failed && journalctl -p 3 -b;'
alias mountkf='sudo cryptsetup open /dev/disk/by-uuid/dd18906b-821e-46cc-9580-60b70ce33737 peter && sudo mount --mkdir -t ext4 /dev/mapper/peter /run/media/peter'
alias umountkf='sudo umount /run/media/peter && sudo cryptsetup close peter'
alias findfat='find ~ -xdev -type f -size +100M'

#    8.4 Kitty aliases

alias icat="kitty +kitten icat"
alias tssh="kitty +kitten ssh"

#    8.5 Misc Programs aliases

alias send='ffsend'
alias grim1='grim -t png -l 0 -o DP-1'
alias grim2='grim -t png -l 0 -o DP-2'
alias grimc='slurp | grim -g - - | wl-copy'

#    8.5 git aliases

alias gitclean='git clean -dxfi'
alias homesync='~/.bin/sh/synchomepage.sh'

#    8.6 Pacman & Auracle aliases

alias get='sudo pacman -S'
alias yap='pacman -Qq | fzf --color="border:magenta,info:green,header:green,info:yellow,hl:blue,label:magenta" --multi --preview "pacman -Qi {1}" --preview-window=up | xargs -ro sudo pacman -Rns'
alias yeet='sudo pacman -Rns'
alias kick='sudo pacman -R'
alias search='pacman -Ss'
alias spot='pacman -Qs'
alias searchaur='auracle search'
alias getaur='auracle clone'
alias listaur='pacman -Qm'
alias buildorder='auracle buildorder'
alias updateaur='auracle update && cd ~/Downloads/AUR'
alias check='pacman -Si'
alias checkaur='auracle info'
alias clear='clear && fastfetch'
alias pkgstatus='comm -23 <(pacman -Qqm | sort) <(curl https://aur.archlinux.org/packages.gz | gzip -cd | sort)'

#    8.7 Script Aliases

alias checkupdates='~/.bin/sh/checkupdates.sh'
alias update='~/.bin/sh/update.sh'
alias reboot='~/.bin/sh/reboot.sh'
alias shutdown='~/.bin/sh/shutdown.sh'
alias pkgb='~/.bin/sh/pkgb.sh'
alias build='~/.bin/sh/build.sh'
alias buildgames='~/.bin/sh/buildgames.sh'
alias deloldbins='rm -rf ~/.bin/*/old/*'
alias orphans='~/.bin/sh/orphans.sh'
alias bday='/home/peter/.bin/sh/bd.sh'
alias kap='~/.bin/sh/kap.sh'
alias lap='~/.bin/sh/lap.sh'
alias aurfzf='~/.bin/sh/aur.sh'

#    8.8 Meme aliases

alias get-waifus='mpv "https://www.youtube.com/watch?v=sr078vS8l_4"'
alias bullyme='mpv "https://www.youtube.com/watch?v=9jrO58mg-Qg"'
alias archbtw='fastfetch'

#    8.9 Newsboat aliases

alias news='~/.bin/sh/news.sh'


#    8.10 Offline Arch Wiki

alias archwiki='lynx https://wiki.archlinux.org/'
alias offlinewiki='lynx /usr/share/doc/arch-wiki/html/en'

# 9. Android tools
export PATH=${PATH}:~/platform-tools

# 10. Arch BTW

fastfetch

# 11. Dracula TTY theme

if [ "$TERM" = "linux" ]; then
	printf %b '\e[40m' '\e[8]' # set default background to color 0 'dracula-bg'
	printf %b '\e[37m' '\e[8]' # set default foreground to color 7 'dracula-fg'
	printf %b '\e]P0282a36'    # redefine 'black'          as 'dracula-bg'
	printf %b '\e]P86272a4'    # redefine 'bright-black'   as 'dracula-comment'
	printf %b '\e]P1ff5555'    # redefine 'red'            as 'dracula-red'
	printf %b '\e]P9ff7777'    # redefine 'bright-red'     as '#ff7777'
	printf %b '\e]P250fa7b'    # redefine 'green'          as 'dracula-green'
	printf %b '\e]PA70fa9b'    # redefine 'bright-green'   as '#70fa9b'
	printf %b '\e]P3f1fa8c'    # redefine 'brown'          as 'dracula-yellow'
	printf %b '\e]PBffb86c'    # redefine 'bright-brown'   as 'dracula-orange'
	printf %b '\e]P4bd93f9'    # redefine 'blue'           as 'dracula-purple'
	printf %b '\e]PCcfa9ff'    # redefine 'bright-blue'    as '#cfa9ff'
	printf %b '\e]P5ff79c6'    # redefine 'magenta'        as 'dracula-pink'
	printf %b '\e]PDff88e8'    # redefine 'bright-magenta' as '#ff88e8'
	printf %b '\e]P68be9fd'    # redefine 'cyan'           as 'dracula-cyan'
	printf %b '\e]PE97e2ff'    # redefine 'bright-cyan'    as '#97e2ff'
	printf %b '\e]P7f8f8f2'    # redefine 'white'          as 'dracula-fg'
	printf %b '\e]PFffffff'    # redefine 'bright-white'   as '#ffffff'
	clear
fi

# 12. FZF dracula theme

export FZF_DEFAULT_OPTS='--color=fg:#f8f8f2,bg:#282a36,hl:#bd93f9 --color=fg+:#f8f8f2,bg+:#44475a,hl+:#bd93f9 --color=info:#ffb86c,prompt:#50fa7b,pointer:#ff79c6 --color=marker:#ff79c6,spinner:#ffb86c,header:#6272a4'

# 13. Dracula Theme for zsh-syntax-highlighting

ZSH_HIGHLIGHT_HIGHLIGHTERS=(main cursor)
typeset -gA ZSH_HIGHLIGHT_STYLES

##    13.1 General
###   13.2 Diffs
###   13.3 Markup
##    13.4 Classes

##    13.5 Comments

ZSH_HIGHLIGHT_STYLES[comment]='fg=#6272A4'

##    13.6 Constants
##    13.7 Entitites

##    13.8 Functions/methods

ZSH_HIGHLIGHT_STYLES[alias]='fg=#50FA7B'
ZSH_HIGHLIGHT_STYLES[suffix-alias]='fg=#50FA7B'
ZSH_HIGHLIGHT_STYLES[global-alias]='fg=#50FA7B'
ZSH_HIGHLIGHT_STYLES[function]='fg=#50FA7B'
ZSH_HIGHLIGHT_STYLES[command]='fg=#50FA7B'
ZSH_HIGHLIGHT_STYLES[precommand]='fg=#50FA7B,italic'
ZSH_HIGHLIGHT_STYLES[autodirectory]='fg=#FFB86C,italic'
ZSH_HIGHLIGHT_STYLES[single-hyphen-option]='fg=#FFB86C'
ZSH_HIGHLIGHT_STYLES[double-hyphen-option]='fg=#FFB86C'
ZSH_HIGHLIGHT_STYLES[back-quoted-argument]='fg=#BD93F9'
##    13.9 Keywords

##    13.10 Built ins

ZSH_HIGHLIGHT_STYLES[builtin]='fg=#8BE9FD'
ZSH_HIGHLIGHT_STYLES[reserved-word]='fg=#8BE9FD'
ZSH_HIGHLIGHT_STYLES[hashed-command]='fg=#8BE9FD'

##    13.11 Punctuation

ZSH_HIGHLIGHT_STYLES[commandseparator]='fg=#FF79C6'
ZSH_HIGHLIGHT_STYLES[command-substitution-delimiter]='fg=#F8F8F2'
ZSH_HIGHLIGHT_STYLES[command-substitution-delimiter-unquoted]='fg=#F8F8F2'
ZSH_HIGHLIGHT_STYLES[process-substitution-delimiter]='fg=#F8F8F2'
ZSH_HIGHLIGHT_STYLES[back-quoted-argument-delimiter]='fg=#FF79C6'
ZSH_HIGHLIGHT_STYLES[back-double-quoted-argument]='fg=#FF79C6'
ZSH_HIGHLIGHT_STYLES[back-dollar-quoted-argument]='fg=#FF79C6'

##    13.12 Serializable / Configuration Languages
##    13.14 Storage

##    13.15 Strings

ZSH_HIGHLIGHT_STYLES[command-substitution-quoted]='fg=#F1FA8C'
ZSH_HIGHLIGHT_STYLES[command-substitution-delimiter-quoted]='fg=#F1FA8C'
ZSH_HIGHLIGHT_STYLES[single-quoted-argument]='fg=#F1FA8C'
ZSH_HIGHLIGHT_STYLES[single-quoted-argument-unclosed]='fg=#FF5555'
ZSH_HIGHLIGHT_STYLES[double-quoted-argument]='fg=#F1FA8C'
ZSH_HIGHLIGHT_STYLES[double-quoted-argument-unclosed]='fg=#FF5555'
ZSH_HIGHLIGHT_STYLES[rc-quote]='fg=#F1FA8C'

##    13.16 Variables

ZSH_HIGHLIGHT_STYLES[dollar-quoted-argument]='fg=#F8F8F2'
ZSH_HIGHLIGHT_STYLES[dollar-quoted-argument-unclosed]='fg=#FF5555'
ZSH_HIGHLIGHT_STYLES[dollar-double-quoted-argument]='fg=#F8F8F2'
ZSH_HIGHLIGHT_STYLES[assign]='fg=#F8F8F2'
ZSH_HIGHLIGHT_STYLES[named-fd]='fg=#F8F8F2'
ZSH_HIGHLIGHT_STYLES[numeric-fd]='fg=#F8F8F2'

##    13.17 No category relevant in spec

ZSH_HIGHLIGHT_STYLES[unknown-token]='fg=#FF5555'
ZSH_HIGHLIGHT_STYLES[path]='fg=#F8F8F2'
ZSH_HIGHLIGHT_STYLES[path_pathseparator]='fg=#FF79C6'
ZSH_HIGHLIGHT_STYLES[path_prefix]='fg=#F8F8F2'
ZSH_HIGHLIGHT_STYLES[path_prefix_pathseparator]='fg=#FF79C6'
ZSH_HIGHLIGHT_STYLES[globbing]='fg=#F8F8F2'
ZSH_HIGHLIGHT_STYLES[history-expansion]='fg=#BD93F9'
#ZSH_HIGHLIGHT_STYLES[command-substitution]='fg=?'
#ZSH_HIGHLIGHT_STYLES[command-substitution-unquoted]='fg=?'
#ZSH_HIGHLIGHT_STYLES[process-substitution]='fg=?'
#ZSH_HIGHLIGHT_STYLES[arithmetic-expansion]='fg=?'
ZSH_HIGHLIGHT_STYLES[back-quoted-argument-unclosed]='fg=#FF5555'
ZSH_HIGHLIGHT_STYLES[redirection]='fg=#F8F8F2'
ZSH_HIGHLIGHT_STYLES[arg0]='fg=#F8F8F2'
ZSH_HIGHLIGHT_STYLES[default]='fg=#F8F8F2'
ZSH_HIGHLIGHT_STYLES[cursor]='standout'

# 14. Source plugins

source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh

# 15. Keybinds

bindkey -v '^?' backward-delete-char # Vi keybindings like God intended
bindkey "\e[3~" delete-char
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down
bindkey -M vicmd 'k' history-substring-search-up
bindkey -M vicmd 'j' history-substring-search-down
