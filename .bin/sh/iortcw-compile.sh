#!/bin/sh

# Switch to git directory and clone source
{
cd /home/peter/Downloads/git;
git clone https://github.com/iortcw/iortcw.git;
}

# Build binary

{
cd /home/peter/Downloads/git/iortcw/SP;
make -j12;
}

# Install executable @ ~/.bin

{
cd /home/peter/.bin/iortcw;
mv /home/peter/.bin/iortcw/* /home/peter/.bin/iortcw/.old;
cp -r /home/peter/Downloads/git/iortcw/SP/build/*/* /home/peter/.bin/iortcw;
cd /home/peter/.bin/iortcw;
chmod 755 /home/peter/.bin/iortcw/iowolfsp.x86_64;
}

# Cleanup

{
rm -rf /home/peter/Downloads/git/iortcw;
}
