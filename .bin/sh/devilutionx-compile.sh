#!/bin/sh

# Switch to git directory and clone source
{
cd /home/peter/Downloads/git;
git clone https://github.com/diasurgical/devilutionX.git;
}

# Build binary

{
cd devilutionX;
cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release -DDEVILUTIONX_SYSTEM_LIBSODIUM=OFF -DDEVILUTIONX_STATIC_LIBSODIUM=ON;
cmake --build build -j $(getconf _NPROCESSORS_ONLN);
}

# Install executable @ ~/.bin

{
cd /home/peter/.bin/devilutionx;
mv /home/peter/.bin/devilutionx/devilutionx /home/peter/.bin/devilutionx/.old;
cp -r /home/peter/Downloads/git/devilutionX/build/devilutionx /home/peter/.bin/devilutionx;
cp -r /mnt/M2/Downloads/git/devilutionX/assets /home/peter/.bin/devilutionx;
chmod 755 /home/peter/.bin/devilutionx/devilutionx;
}

# Cleanup

{
rm -rf /home/peter/Downloads/git/devilutionX;
}
