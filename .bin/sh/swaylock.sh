#!/bin/sh
swaylock -f -i DP-1:~/.wallpapers/castlevania.png -i DP-2:~/.wallpapers/castlevania2.png --inside-color "#44475a" --inside-clear-color "#50fa7b" --ring-color "#ff79c6" --text-caps-lock-color "#f8f8f2" --ring-caps-lock-color "#f8f8f2" --key-hl-color "#50fa7b"
