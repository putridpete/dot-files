#!/bin/sh

# Switch to git directory and clone source
{
cd /home/peter/Downloads/git;
git clone https://bitbucket.org/ecwolf/ecwolf.git;
}

# Build binary

{
mkdir -pv /home/peter/Downloads/git/ecwolf/build;
cd /home/peter/Downloads/git/ecwolf/build;
cmake .. -DCMAKE_BUILD_TYPE=Release -DGPL=ON;
make;
}

# Install executable @ ~/.bin

{
cd /home/peter/.bin/ecwolf;
mv /home/peter/.bin/ecwolf/ecwolf /home/peter/.bin/ecwolf/.old;
mv /home/peter/.bin/ecwolf/ecwolf.pk3 /home/peter/.bin/ecwolf/.old;
cp /home/peter/Downloads/git/ecwolf/build/ecwolf /home/peter/.bin/ecwolf;
cp /home/peter/Downloads/git/ecwolf/build/ecwolf.pk3 /home/peter/.bin/ecwolf;
cd /home/peter/.bin/ecwolf;
chmod 755 /home/peter/.bin/ecwolf/ecwolf;
}

# Cleanup

{
rm -rf /home/peter/Downloads/git/ecwolf;
}
