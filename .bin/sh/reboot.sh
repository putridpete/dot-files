#!/bin/sh

# Prompt Reboot

{
read -p "Reboot now? [y/n]:" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    command shutdown -r now
fi
}
