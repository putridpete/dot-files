#!/bin/sh

# Switch to git directory and clone source
{
cd /home/peter/Downloads/git;
git clone https://github.com/Shpoike/Quakespasm.git;
}

# Build binary

{
cd /home/peter/Downloads/git/Quakespasm/Quake;
make;
}

# Install executable @ ~/.bin

{
cd /home/peter/.bin/Quakespasm;
mv /home/peter/.bin/Quakespasm/quakespasm /home/peter/.bin/Quakespasm/.old;
mv /home/peter/.bin/Quakespasm/quakespasm.pak /home/peter/.bin/Quakespasm/.old;
cd /home/peter/Downloads/git/Quakespasm/Quake;
cp /home/peter/Downloads/git/Quakespasm/Quake/quakespasm /home/peter/.bin/Quakespasm;
cp /home/peter/Downloads/git/Quakespasm/Quake/quakespasm.pak /home/peter/.bin/Quakespasm;
chmod 755 /home/peter/.bin/Quakespasm;
}

# Cleanup

{
rm -rf /home/peter/Downloads/git/Quakespasm;
}
