#!/bin/sh

# Switch to git directory and clone source
{
cd /home/peter/Downloads/git;
git clone https://github.com/ArnoAnsems/CatacombGL;
}

# Build binary

{
cd CatacombGL;
mkdir build;
}

# Install executable @ ~/.bin

{
cd build;
mv /home/peter/.bin/catacombgl/CatacombGL /home/peter/.bin/catacombgl/.old;
cmake ..;
make -j12;
cp /home/peter/Downloads/git/CatacombGL/build/CatacombGL /home/peter/.bin/catacombgl;
chmod 755 /home/peter/.bin/catacombgl/CatacombGL;
}

# Cleanup

{
rm -rf /home/peter/Downloads/git/CatacombGL;
}
