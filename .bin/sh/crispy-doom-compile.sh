#!/bin/sh

# Switch to git directory and clone source
{
cd /home/peter/Downloads/git;
git clone https://github.com/fabiangreffrath/crispy-doom.git;
}

# Build binary

{
cd crispy-doom;
autoreconf -fiv;
./configure;
make;
}

# Install executable @ ~/.bin

{
cd /home/peter/.bin/crispy-doom;
mv /home/peter/.bin/crispy-doom/crispy-doom /home/peter/.bin/crispy-doom/.old;
mv /home/peter/.bin/crispy-heretic/crispy-heretic /home/peter/.bin/crispy-heretic/.old;
mv /home/peter/.bin/crispy-hexen/crispy-hexen /home/peter/.bin/crispy-hexen/.old;
mv /home/peter/.bin/crispy-doom/crispy-doom-setup /home/peter/.bin/crispy-doom/.old;
mv /home/peter/.bin/crispy-heretic/crispy-heretic-setup /home/peter/.bin/crispy-heretic/.old;
mv /home/peter/.bin/crispy-hexen/crispy-hexen-setup /home/peter/.bin/crispy-hexen/.old;
cd /home/peter/Downloads/git/crispy-doom;
cp /home/peter/Downloads/git/crispy-doom/src/crispy-doom /home/peter/.bin/crispy-doom;
cp /home/peter/Downloads/git/crispy-doom/src/crispy-heretic /home/peter/.bin/crispy-heretic;
cp /home/peter/Downloads/git/crispy-doom/src/crispy-hexen /home/peter/.bin/crispy-hexen;
cp /home/peter/Downloads/git/crispy-doom/src/crispy-doom-setup /home/peter/.bin/crispy-doom;
cp /home/peter/Downloads/git/crispy-doom/src/crispy-heretic-setup /home/peter/.bin/crispy-heretic;
cp /home/peter/Downloads/git/crispy-doom/src/crispy-hexen-setup /home/peter/.bin/crispy-hexen;
chmod 755 /home/peter/.bin/crispy-doom/crispy-doom;
chmod 755 /home/peter/.bin/crispy-heretic/crispy-heretic;
chmod 755 /home/peter/.bin/crispy-hexen/crispy-hexen;
}

# Cleanup

{
rm -rf /home/peter/Downloads/git/crispy-doom;
}
