#!/bin/sh

# Check for orphans then asks for removal

{
while [[ $(pacman -Qtdq) ]];
 do
   pacman -Qtdq;
   read -p "Remove orphaned packages? [y/n]:" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
   sudo pacman -R $(pacman -Qtdq) 
elif [[ -n $(pacman -Qtdq) ]];
 then
   exit
fi
done
}
