#!/bin/sh

# Switch to git directory and clone source
{
cd /home/peter/Downloads/git;
git clone https://github.com/atsb/Doom64EX-Plus.git;
}

# Build binary

{
cd /home/peter/Downloads/git/Doom64EX-Plus/src/engine;
bash /home/peter/Downloads/git/Doom64EX-Plus/src/engine/build.sh;
}

# Install executable @ ~/.bin

{
cd /home/peter/.bin/Doom64EX-Plus;
mv /home/peter/.bin/Doom64EX-Plus/DOOM64EX-Plus /home/peter/.bin/Doom64EX-Plus/.old;
cp /home/peter/Downloads/git/Doom64EX-Plus/src/engine/DOOM64EX-Plus /home/peter/.bin/Doom64EX-Plus;
chmod 755 /home/peter/.bin/Doom64EX-Plus/DOOM64EX-Plus;
}

# Cleanup

{
rm -rf /home/peter/Downloads/git/Doom64EX-Plus;
}
