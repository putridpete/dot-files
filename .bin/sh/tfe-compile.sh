#!/bin/sh

# Switch to git directory and clone source
{
rm -rf /home/peter/.bin/tfe/TheForceEngine 
mv /home/peter/.bin/tfe/* /home/peter/.bin/tfe/.old;
cd /home/peter/.bin/tfe;
git clone https://github.com/luciusDXL/TheForceEngine.git;
mkdir TheForceEngine/tfe-build; cd TheForceEngine/tfe-build;
}

# Build binary

{
cmake -S /home/peter/.bin/tfe/TheForceEngine;
make -j12;
}

# Install executable @ ~/.bin

{
cd /home/peter/.bin/tfe;
cp -r /home/peter/.bin/tfe/TheForceEngine/tfe-build/* /home/peter/.bin/tfe;
chmod 755 /home/peter/.bin/tfe/TheForceEngine/tfe-build/theforceengine;
}

