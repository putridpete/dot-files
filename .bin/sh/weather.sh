#!/usr/bin/env bash
# weather.sh
for i in {1..5}
do
    text=$(curl -s "https://v2.wttr.in/$1?format=1&m")
    if [[ $? == 0 ]]
    then
        text=$(echo "$text" | sed -E "s/\s+/ /g")
        tooltip=$(echo -ne "Santo Domingo, Dominican Republic:";curl -s "https://wttr.in/$1?format=2&m")
        if [[ $? == 0 ]]
        then
            tooltip=$(echo "$tooltip" | sed -E "s/\s+/ /g")
            echo "{\"text\":\"$text\", \"tooltip\":\"$tooltip\"}"
            exit
        fi
    fi
    sleep 2
done
echo "{\"text\":\"error\", \"tooltip\":\"error\"}"
