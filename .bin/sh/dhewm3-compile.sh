#!/bin/sh

# Switch to git directory and clone source
{
cd /home/peter/Downloads/git;
git clone https://github.com/dhewm/dhewm3.git;
}

# Build binary

{
mkdir /home/peter/Downloads/git/dhewm3/build;
cd /home/peter/Downloads/git/dhewm3/build;
cmake ../neo/;
make -j12;
}

# Install executable @ ~/.bin

{
cd /home/peter/.bin/dhewm3;
mv /home/peter/.bin/dhewm3/dhewm3 /home/peter/.bin/dhewm3/.old;
mv /home/peter/.bin/dhewm3/base.so /home/peter/.bin/dhewm3/.old;
mv /home/peter/.bin/dhewm3/d3xp.so /home/peter/.bin/dhewm3/.old;
cp /home/peter/Downloads/git/dhewm3/build/dhewm3 /home/peter/.bin/dhewm3;
cp /home/peter/Downloads/git/dhewm3/build/base.so /home/peter/.bin/dhewm3;
cp /home/peter/Downloads/git/dhewm3/build/d3xp.so /home/peter/.bin/dhewm3;
cd /home/peter/.bin/dhewm3;
chmod 755 /home/peter/.bin/dhewm3/dhewm3;
}

# Cleanup

{
rm -rf /home/peter/Downloads/git/dhewm3;
}
