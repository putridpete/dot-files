#!/bin/sh

# Ask for confirmation to build nblood

{
read -p "Build nblood? [y/n]:" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    ~/.bin/sh/nblood-compile.sh
fi 
}

# Ask for confirmation to build rednukem

{
read -p "Build rednukem? [y/n]:" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    ~/.bin/sh/rednukem-compile.sh
fi 
}


# Ask for confirmation to build voidsw

{
read -p "Build voidsw? [y/n]:" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    ~/.bin/sh/voidsw-compile.sh
fi 
}

# Ask for confirmation to build eduke32

{
read -p "Build eduke32? [y/n]:" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    ~/.bin/sh/ionfury-compile.sh
fi 
}

# Ask for confirmation to build vkquake

{
read -p "Build Quakespasm-spiked? [y/n]:" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    ~/.bin/sh/quakespasm-compile.sh
fi 
}

# Ask for confirmation to build crispy-doom

{
read -p "Build crispy-doom? [y/n]:" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    ~/.bin/sh/crispy-doom-compile.sh
fi 
}
