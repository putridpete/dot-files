#!/bin/sh

# Perform update

sudo pacman -Syu

# Check pacdiff

{
read -p "Execute pacdiff? [y/n]:" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    pacdiff -s
fi
}
# Check for orphans
{
read -p "Check for orphan packages? [y/n]:" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    sudo bash /home/peter/.bin/sh/orphans.sh
fi
}
# Update the Ollama model

read -p "Would you like to update your ollama model? [y/n]:" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    echo "Updating ollama model..."
    ollama pull llama3.1
fi
# Prompt reboot

~/.bin/sh/reboot.sh
