#!/bin/sh

# Switch to git directory and clone source
{
mv /home/peter/.bin/openjkdf2/OpenJKDF2 /home/peter/.bin/openjkdf2/.old;
cd /home/peter/.bin/openjkdf2;
git clone https://github.com/shinyquagsire23/OpenJKDF2.git;
cd OpenJKDF2;
}

# Build binary

{
git submodule update --init
export CC=clang;
export CXX=clang++;
chmod +x build_linux64.sh;
./build_linux64.sh;
}

# Install executable @ ~/.bin

{
chmod 755 /home/peter/.bin/openjkdf2/OpenJKDF2/build_linux64/openjkdf2;
}
