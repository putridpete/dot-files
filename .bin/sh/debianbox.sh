#!/bin/sh

# Ask to connect to server via SSH

{
read -p "Connect to server via SSH? [y/n]:" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    ssh kitty +kitten ssh debianbox
fi 
}

