#!/bin/sh

# Switch to git directory and clone source
{
cd /home/peter/Downloads/git;
git clone https://github.com/nukeykt/NBlood.git;
}

# Build binary

{
cd NBlood;
make rednukem;
}

# Install executable @ ~/.bin

{
cd /home/peter/.bin/Rednukem;
mv /home/peter/.bin/Rednukem/rednukem /home/peter/.bin/Rednukem/.old;
mv /home/peter/.bin/Rednukem/dn64widescreen.pk3 /home/peter/.bin/Rednukem/.old;
cd /home/peter/Downloads/git/NBlood;
cp /home/peter/Downloads/git/NBlood/rednukem /home/peter/.bin/Rednukem;
cp /home/peter/Downloads/git/NBlood/dn64widescreen.pk3 /home/peter/.bin/Rednukem;
chmod 755 /home/peter/.bin/Rednukem/rednukem;
}

# Cleanup

{
rm -rf /home/peter/Downloads/git/NBlood;
}
